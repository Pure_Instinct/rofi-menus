#!/bin/bash
mkdir ~/.config/rofi
cp config.rasi ~/.config/rofi
cp scripts -r ~/.config/rofi
cp themes -r ~/.config/rofi
chmod +x ~/.config/rofi/scripts/ -R
echo "Now installing dependencies."
yay -S --noconfirm mpc mpd libnotify ttf-comfortaa maim rofi
# remount /tmp so it's big enough to compile nerd-fonts-complete
sudo mount -o remount,size=10G,noatime /tmp && yay -S nerd-fonts-complete # Comment this line if you want to install fonts manually
echo "Installation completed."
echo "Sample config for i3/bspwm can be found in README.md"
